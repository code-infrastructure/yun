package com.faner4cloud.yun.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ...zz
 * @version v1
 * @summary 字典类型
 * @since 2022/5/1 4:05 PM
 */
@Data
@ApiModel(value = "字典类型")
@EqualsAndHashCode(callSuper = true)
public class SysDict extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(name = "字典编号")
	private Long id;

	/**
	 * 类型
	 */
	@ApiModelProperty(name = "字典类型")
	private String type;

	/**
	 * 描述
	 */
	@ApiModelProperty(name = "字典描述")
	private String description;

	/**
	 * 是否是系统内置
	 */
	@ApiModelProperty(name = "是否系统内置")
	private String systemFlag;

	/**
	 * 备注信息
	 */
	@ApiModelProperty(name = "备注信息")
	private String remark;

	/**
	 * 删除标记
	 */
	@TableLogic
	@ApiModelProperty(name = "删除标记,1:已删除,0:正常")
	private Integer delFlag;

}
