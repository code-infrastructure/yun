package com.faner4cloud.yun.dao.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.faner4cloud.yun.common.util.DateUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 点赞表（关联帖子、一级评论）
 *
 * @author faner
 * @date 2021-11-26 10:40:31
 */
@Data
@TableName("t_like")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "点赞表（关联帖子、一级评论）")
public class Like extends Model<Like> {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId
    @ApiModelProperty(value = "主键")
    private Long likeId;
    /**
     * 类别 1-帖子 2-评论
     */
    @ApiModelProperty(value = "类别 1-帖子 2-评论")
    private Integer type;
    /**
     * 类别ID
     */
    @ApiModelProperty(value = "类别ID")
    private Long typeId;
    /**
     * 浏览人id
     */
    @ApiModelProperty(value = "浏览人id")
    private Long userId;

    /**
     * 点赞时间
     */
    @ApiModelProperty(value = "点赞时间")
    private Date likeTime;

    /**
     * 状态 1-点赞 2-取赞
     */
    @ApiModelProperty(value = "状态 1-点赞 2-取赞")
    private Integer status;
}
