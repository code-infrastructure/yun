package com.faner4cloud.yun.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.faner4cloud.yun.dao.entity.SysDict;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ...zz
 * @version v1
 * @summary 字典表 Mapper 接口
 * @since 2022/5/1 4:29 PM
 */
@Mapper
public interface SysDictMapper extends BaseMapper<SysDict> {

}
