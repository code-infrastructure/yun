package com.faner4cloud.yun.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/1 4:00 PM
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUserRole extends Model<SysUserRole> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.ASSIGN_ID)
	@ApiModelProperty(name = "主键Id")
	private Long id;

	/**
	 * 用户ID
	 */
	@ApiModelProperty(name = "用户id")
	private Long userId;

	/**
	 * 角色ID
	 */
	@ApiModelProperty(name = "角色id")
	private Long roleId;

}
