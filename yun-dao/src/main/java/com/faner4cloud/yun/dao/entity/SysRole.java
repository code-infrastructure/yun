package com.faner4cloud.yun.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/1 3:48 PM
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
public class SysRole extends BaseEntity {

	private static final long serialVersionUID = 1L;

	@TableId(value = "role_id", type = IdType.ASSIGN_ID)
	@ApiModelProperty(name = "角色编号")
	private Long roleId;

	@NotBlank(message = "角色名称 不能为空")
	@ApiModelProperty(name = "角色名称")
	private String roleName;

	@NotBlank(message = "角色标识 不能为空")
	@ApiModelProperty(name = "角色标识")
	private String roleCode;

	@NotBlank(message = "角色描述 不能为空")
	@ApiModelProperty(name = "角色描述")
	private String roleDesc;

	/**
	 * 删除标识（0-正常,1-删除）
	 */
	@TableLogic
	private Integer delFlag;

}
