package com.faner4cloud.yun.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/1 3:56 PM
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysUser extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键ID
	 */
	@TableId(value = "user_id", type = IdType.ASSIGN_ID)
	@ApiModelProperty(name = "主键id")
	private Long userId;

	/**
	 * 用户名
	 */
	@ApiModelProperty(name = "用户名")
	private String username;

	/**
	 * 密码
	 */
	@ApiModelProperty(name = "密码")
	private String password;

	/**
	 * 随机盐
	 */
	@JsonIgnore
	@ApiModelProperty(name = "随机盐")
	private String salt;

	/**
	 * 锁定标记
	 */
	@ApiModelProperty(name = "锁定标记")
	private Integer lockFlag;

	/**
	 * 手机号
	 */
	@ApiModelProperty(name = "手机号")
	private String phone;

	/**
	 * 头像
	 */
	@ApiModelProperty(name = "头像地址")
	private String avatar;

	/**
	 * 部门ID
	 */
	@ApiModelProperty(name = "用户所属部门id")
	private Long deptId;

	/**
	 * 0-正常，1-删除
	 */
	@TableLogic
	private Integer delFlag;

}
