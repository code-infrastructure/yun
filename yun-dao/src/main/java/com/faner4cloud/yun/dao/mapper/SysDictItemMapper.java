package com.faner4cloud.yun.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.faner4cloud.yun.dao.entity.SysDictItem;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ...zz
 * @version v1
 * @summary 字典项
 * @since 2022/5/1 4:31 PM
 */
@Mapper
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {
	
}
