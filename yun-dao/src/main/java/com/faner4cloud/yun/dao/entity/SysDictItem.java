package com.faner4cloud.yun.dao.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author ...zz
 * @version v1
 * @summary 字典项
 * @since 2022/5/1 4:21 PM
 */
@Data
@ApiModel(value = "字典项")
@EqualsAndHashCode(callSuper = true)
public class SysDictItem extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	@TableId(type = IdType.ASSIGN_ID)
	@ApiModelProperty(name = "字典项id")
	private Long id;

	/**
	 * 所属字典类id
	 */
	@ApiModelProperty(name = "所属字典类id")
	private Long dictId;

	/**
	 * 数据值
	 */
	@ApiModelProperty(name = "数据值")
	private String value;

	/**
	 * 标签名
	 */
	@ApiModelProperty(name = "标签名")
	private String label;

	/**
	 * 类型
	 */
	@ApiModelProperty(name = "类型")
	private String type;

	/**
	 * 描述
	 */
	@ApiModelProperty(name = "描述")
	private String description;

	/**
	 * 排序（升序）
	 */
	@ApiModelProperty(name = "排序值，默认升序")
	private Integer sortOrder;

	/**
	 * 备注信息
	 */
	@ApiModelProperty(name = "备注信息")
	private String remark;

	/**
	 * 删除标记
	 */
	@TableLogic
	@ApiModelProperty(name = "删除标记,1:已删除,0:正常")
	private Integer delFlag;

}
