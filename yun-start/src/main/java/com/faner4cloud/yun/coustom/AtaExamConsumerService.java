package com.faner4cloud.yun.coustom;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author
 * @version v1
 * @since 2021-04-25 13:25
 */
@Slf4j
@Service
public class AtaExamConsumerService {

    /**
     * 表驱动
     */
    @Getter
    private Map<Integer, Consumer<KafkaAtaOpsDto>> dealConsumerMap = new HashMap<>();


    /**
     * @version v1
     * @summary 初始化执行方法
     * @since 2021-04-25 13:25
     */
    @PostConstruct
    public void init() {
        dealConsumerMap.put(KafkaAtaOpsDto.OpsTypeEnum.打印准考证消息.getCode(), ataAdmissionTicket);
    }

    /**
     * ata
     * 准考证打印通知
     */
    private final Consumer<KafkaAtaOpsDto> ataAdmissionTicket = kafkaAtaOpsDtoV1 -> {
        try {
            log.info("打印准考证ataAdmissionTicket - kafkaAtaOpsDtoV1.getDataDetail():{}", kafkaAtaOpsDtoV1.getDataDetail());
        } catch (Exception e) {
            log.error("[准考证打印通知]收到消息[{}], 不是本次需要的消息格式:{},", KafkaAtaOpsDto.OpsTypeEnum.打印准考证消息.name(), kafkaAtaOpsDtoV1.getDataDetail(), e);
        }
    };
}
