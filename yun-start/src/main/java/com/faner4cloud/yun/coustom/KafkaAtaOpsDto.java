package com.faner4cloud.yun.coustom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author luck(23092676)
 * @version v1
 * @summary ata操作的
 * @since 2020-05-21 17:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KafkaAtaOpsDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String dataDetail;
    /**
     * @see OpsTypeEnum
     */
    private int opsType;

    @Getter
    @AllArgsConstructor
    @SuppressWarnings("NonAsciiCharacters")
    public enum OpsTypeEnum {
        /**
         * 消息类型
         */
        打印准考证消息(0),
        ATA考场预约消息(1);

        private int code;
    }
}
