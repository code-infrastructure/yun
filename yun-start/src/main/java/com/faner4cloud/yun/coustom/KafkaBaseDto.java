package com.faner4cloud.yun.coustom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author luck(23092676)
 * @version v1
 * @summary kafka的消息体
 * @since 2020-04-21 21:07
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class KafkaBaseDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 消息类型
     */
    private int type;
    /**
     * 数据
     */
    private String data;
}
