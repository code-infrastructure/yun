package com.faner4cloud.yun.coustom;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/5 10:08 AM
 */
@Slf4j
@Service
public class AtaConsumerService {
	/**
	 * 表驱动
	 */
	@Getter
	private Map<Integer, Consumer<KafkaAtaOpsDto>> dealConsumerMap = new HashMap<>();
	/**
	 * @version v1
	 * @summary 初始化执行方法
	 * @since 2020-04-23 10:13
	 */
	@PostConstruct
	public void init() {
		dealConsumerMap.put(KafkaAtaOpsDto.OpsTypeEnum.ATA考场预约消息.getCode(), ataExamNotice);
	}

	/**
	 * ATA考场预约消息处理
	 * !important 这里的kafka消息是无序的，预约考场的消息可能晚于取消预约的消息发送
	 */
	private final Consumer<KafkaAtaOpsDto> ataExamNotice = kafkaAtaOpsDtoV1 -> {
		try {
			log.info("ATA预约消息: {}", kafkaAtaOpsDtoV1.getDataDetail());
		} catch (Exception e) {
			log.error("收到消息[{}], 不是本次需要的消息格式:{}", KafkaAtaOpsDto.OpsTypeEnum.ATA考场预约消息.name(), kafkaAtaOpsDtoV1.getDataDetail(), e);
		}
	};
}
