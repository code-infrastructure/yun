package com.faner4cloud.yun.coustom;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author haibo.gu
 * @date 2020年08月26日
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("NonAsciiCharacters")
public enum KafkaBaseDtoTypeEnum {
    /**
     * 消息类型
     */
    处理搏学同步任务(0),
    ATA逻辑处理(1),
    准考证打印通知处理(3),
    新增搏学存储搏学对应的业务组织树(2);

    private int code;
}
