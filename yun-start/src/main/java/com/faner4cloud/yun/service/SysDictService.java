package com.faner4cloud.yun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.faner4cloud.yun.common.util.progress.Result;
import com.faner4cloud.yun.dao.entity.SysDict;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/10 2:28 PM
 */
public interface SysDictService extends IService<SysDict> {

	/**
	 * 根据ID 删除字典
	 * @param id
	 * @return
	 */
	void removeDict(Long id);

	/**
	 * 更新字典
	 * @param sysDict 字典
	 * @return
	 */
	void updateDict(SysDict sysDict);

	/**
	 * 清除缓存
	 */
	void clearDictCache();

}
