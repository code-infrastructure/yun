package com.faner4cloud.yun.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.faner4cloud.yun.common.constant.CacheConstants;
import com.faner4cloud.yun.common.constant.enums.DictTypeEnum;
import com.faner4cloud.yun.dao.entity.SysDict;
import com.faner4cloud.yun.dao.entity.SysDictItem;
import com.faner4cloud.yun.dao.mapper.SysDictItemMapper;
import com.faner4cloud.yun.service.SysDictItemService;
import com.faner4cloud.yun.service.SysDictService;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/7/8 4:27 PM
 */
@Service
@RequiredArgsConstructor
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {

	private final SysDictService dictService;

	/**
	 * 删除字典项
	 * @param id 字典项ID
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstants.DICT_DETAILS, allEntries = true)
	public void removeDictItem(Long id) {
		// 根据ID查询字典ID
		SysDictItem dictItem = this.getById(id);
		SysDict dict = dictService.getById(dictItem.getDictId());
		// 系统内置
		Assert.state(!DictTypeEnum.SYSTEM.getType().equals(dict.getSystemFlag()),
			"系统内置");
		this.removeById(id);
	}

	/**
	 * 更新字典项
	 * @param item 字典项
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstants.DICT_DETAILS, key = "#item.type")
	public void updateDictItem(SysDictItem item) {
		// 查询字典
		SysDict dict = dictService.getById(item.getDictId());
		// 系统内置
		Assert.state(!DictTypeEnum.SYSTEM.getType().equals(dict.getSystemFlag()),
			"系统内置");
		this.updateById(item);
	}

}

