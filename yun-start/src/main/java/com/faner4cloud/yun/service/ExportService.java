package com.faner4cloud.yun.service;

import com.faner4cloud.plugin.excel.annotation.ResponseExcel;
import com.faner4cloud.yun.common.util.progress.Result;
import com.faner4cloud.yun.dao.entity.SysDict;

import java.util.List;

/**
 * @author ...zz
 * @version v1
 * @summary 导出案例选择
 * @since 2022/5/16 9:03 AM
 */
public interface ExportService {

	/**
	 * 异步轮询方式导出 可以有进度条的显示
	 * @param param 参数
	 * @return {@link Result}
	 */
	@ResponseExcel
	Result asyncExport(String param);
	@ResponseExcel
	Result asyncExport(String param,Boolean a);
	/**
	 * 同步导出
	 * 1.数据量较少的导出
	 * @param param 参数
	 * @return {@link Result}
	 */
	List<SysDict> syncExport();

}
