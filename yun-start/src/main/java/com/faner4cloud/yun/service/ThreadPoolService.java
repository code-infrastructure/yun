package com.faner4cloud.yun.service;

/**
 * @author ...zz
 * @version v1
 * @summary 多线程用例
 * @since 2022/7/11 2:30 PM
 */
public interface ThreadPoolService {
	/**
	 * 使用异步注解 @Async
	 * 使用自定义线程池 {@link com.faner4cloud.yun.common.config.TaskExecutorConfiguration}
	 */
 	void useAsyncExec();

	/**
	 * 多线程并发执行阻塞等待获取结果
	 */
	void blockAsyncExec();
}
