package com.faner4cloud.yun.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.faner4cloud.yun.dao.entity.SysDictItem;

/**
 * @author ...zz
 * @version v1
 * @summary 字典项
 * @since 2022/7/8 4:26 PM
 */
public interface SysDictItemService extends IService<SysDictItem> {

	/**
	 * 删除字典项
	 * @param id 字典项ID
	 * @return
	 */
	void removeDictItem(Long id);

	/**
	 * 更新字典项
	 * @param item 字典项
	 * @return
	 */
	void updateDictItem(SysDictItem item);

}
