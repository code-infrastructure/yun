package com.faner4cloud.yun.service.impl;

import cn.hutool.crypto.digest.MD5;
import com.alibaba.excel.ExcelWriter;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.faner4cloud.plugin.excel.annotation.ResponseExcel;
import com.faner4cloud.yun.common.constant.CacheConstants;
import com.faner4cloud.yun.common.constant.enums.CacheEnum;
import com.faner4cloud.yun.common.constant.enums.DictTypeEnum;
import com.faner4cloud.yun.common.util.SpringContextHolder;
import com.faner4cloud.yun.common.util.progress.Progress;
import com.faner4cloud.yun.common.util.progress.ProgressBuilder;
import com.faner4cloud.yun.common.util.progress.Result;
import com.faner4cloud.yun.common.util.progress.TaskBuilder;
import com.faner4cloud.yun.dao.entity.SysDict;
import com.faner4cloud.yun.dao.entity.SysDictItem;
import com.faner4cloud.yun.dao.mapper.SysDictItemMapper;
import com.faner4cloud.yun.dao.mapper.SysDictMapper;
import com.faner4cloud.yun.service.SysDictService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/10 2:35 PM
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements SysDictService {

	private final SysDictItemMapper dictItemMapper;

	/**
	 * 根据ID 删除字典
	 * @param id 字典ID
	 * @return
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	@CacheEvict(value = CacheConstants.DICT_DETAILS, allEntries = true)
	public void removeDict(Long id) {
		SysDict dict = this.getById(id);
		// 系统内置
		Assert.state(!DictTypeEnum.SYSTEM.getType().equals(dict.getSystemFlag()), "系统内置");
		baseMapper.deleteById(id);
		dictItemMapper.delete(Wrappers.<SysDictItem>lambdaQuery().eq(SysDictItem::getDictId, id));
	}

	/**
	 * 更新字典
	 * @param dict 字典
	 * @return
	 */
	@Override
	@CacheEvict(value = CacheConstants.DICT_DETAILS, key = "#dict.type")
	public void updateDict(SysDict dict) {
		SysDict sysDict = this.getById(dict.getId());
		// 系统内置
		Assert.state(!DictTypeEnum.SYSTEM.getType().equals(sysDict.getSystemFlag()),
			"系统内置");
		this.updateById(dict);
	}

	@Override
	@CacheEvict(value = CacheConstants.DICT_DETAILS, allEntries = true)
	public void clearDictCache() {

	}


}
