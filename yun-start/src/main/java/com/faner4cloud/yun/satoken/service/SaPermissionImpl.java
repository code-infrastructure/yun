package com.faner4cloud.yun.satoken.service;

import cn.dev33.satoken.stp.StpInterface;
import com.faner4cloud.yun.common.constant.enums.UserTypeEnum;
import com.faner4cloud.yun.satoken.LoginHelper;
import com.faner4cloud.yun.satoken.bo.LoginUserBO;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ...zz
 * @version v1
 * @summary sa-token 权限管理实现类
 * @since 2022/8/17 10:06 AM
 */
@Component
public class SaPermissionImpl implements StpInterface {

	/**
	 * 获取菜单权限列表
	 */
	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		LoginUserBO loginUser = LoginHelper.getLoginUser();
		UserTypeEnum userType = UserTypeEnum.getUserType(loginUser.getUserType());
		if (userType == UserTypeEnum.SYS_USER) {
			return new ArrayList<>(loginUser.getMenuPermission());
		} else if (userType == UserTypeEnum.APP_USER) {
			// 其他端 自行根据业务编写
		}
		return new ArrayList<>();
	}

	/**
	 * 获取角色权限列表
	 */
	@Override
	public List<String> getRoleList(Object loginId, String loginType) {
		LoginUserBO loginUser = LoginHelper.getLoginUser();
		UserTypeEnum userType = UserTypeEnum.getUserType(loginUser.getUserType());
		if (userType == UserTypeEnum.SYS_USER) {
			return new ArrayList<>(loginUser.getRolePermission());
		} else if (userType == UserTypeEnum.APP_USER) {
			// 其他端 自行根据业务编写
		}
		return new ArrayList<>();
	}
}

