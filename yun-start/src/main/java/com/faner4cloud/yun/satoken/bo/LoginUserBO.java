package com.faner4cloud.yun.satoken.bo;

import com.faner4cloud.yun.satoken.LoginHelper;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
/**
 * @author ...zz
 * @version v1
 * @summary 登录用户身份权限
 * @since 2022/8/17 10:11 AM
 */
@Data
@NoArgsConstructor
public class LoginUserBO implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 部门ID
	 */
	private Long deptId;

	/**
	 * 部门名
	 */
	private String deptName;

	/**
	 * 用户唯一标识
	 */
	private String token;

	/**
	 * 用户类型
	 */
	private String userType;

	/**
	 * 登录时间
	 */
	private Long loginTime;

	/**
	 * 过期时间
	 */
	private Long expireTime;

	/**
	 * 登录IP地址
	 */
	private String ipaddr;

	/**
	 * 登录地点
	 */
	private String loginLocation;

	/**
	 * 浏览器类型
	 */
	private String browser;

	/**
	 * 操作系统
	 */
	private String os;

	/**
	 * 菜单权限
	 */
	private Set<String> menuPermission;

	/**
	 * 角色权限
	 */
	private Set<String> rolePermission;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 角色对象
	 */
//	private List<RoleDTO> roles;

	/**
	 * 数据权限 当前角色ID
	 */
	private Long roleId;

	/**
	 * 获取登录id
	 */
	public String getLoginId() {
		return userType + LoginHelper.JOIN_CODE + userId;
	}
}
