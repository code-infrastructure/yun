package com.faner4cloud.yun.queue.delay.handler.impl;

import com.faner4cloud.yun.queue.delay.handler.DelayTypeHandlerStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 订单自动取消延迟类
 *
 * @author ...zz
 * @since 2022-04-14
 */
@Slf4j
@Component
public class OrderPayAutoCancelDelayTypeHandler extends DelayTypeHandlerStrategy {


	@Resource
	private RedisTemplate<String, String> redisTemplate;

	//业务去重复key
	private static final String DELAY_KEY = "fighting:delay:pay_auto_cancel:";


	private static final Executor EXECUTOR_SYNC_PAY_AUTO_CANCEL = new ThreadPoolExecutor(
		4, 4, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100), new ThreadFactory() {
		private final AtomicInteger threadNumber = new AtomicInteger(1);

		@SuppressWarnings("NullableProblems")
		@Override
		public Thread newThread(Runnable r) {
			return new Thread(r, "[executor-sync-pay-auto-cancel]-" + threadNumber.getAndIncrement());
		}
	}, new ThreadPoolExecutor.CallerRunsPolicy());

	@Override
	public void execute(String delayTypeValue) {
		if (StringUtils.isEmpty(delayTypeValue)) {
			return;
		}
		log.info("#OrderPayAutoDelayTypeHandler#订单自动取消,消费值：:{}", delayTypeValue);
		try {

		} catch (Exception e) {
			log.error("#OrderPayAutoDelayTypeHandler#订单自动取消失败", e.getMessage());
		}
	}


}
