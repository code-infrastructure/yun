package com.faner4cloud.yun.queue.delay;

/**
 * 延迟阻塞队列 抽象层
 * @author  faner
 * @since 2022-04-14
 */
public interface RedisDelayQueueManager<V> {


    /**
     * 添加到一个延迟队列中
     *
     * @param delayEvent  {@link DelayEvent}
     */
    void put(DelayEvent delayEvent);

    /**
     * 从延迟队列中弹出过期的value
     *
     * @param delayTypeEnum {@link DelayTypeEnum}
     * @return 拉取到的值
     */
    String popup(DelayTypeEnum delayTypeEnum);


}
