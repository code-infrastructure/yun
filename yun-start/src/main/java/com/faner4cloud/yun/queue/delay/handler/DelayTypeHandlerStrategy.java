package com.faner4cloud.yun.queue.delay.handler;

import org.springframework.stereotype.Component;

/**
 * 延迟策略类
 * @author  faner
 * @since 2022-04-14
 */
@Component
public class DelayTypeHandlerStrategy {

    /**
     * 执行类
     * @param delayTypeValue 拉取的值
     */
    public void execute(String delayTypeValue){

    }
}
