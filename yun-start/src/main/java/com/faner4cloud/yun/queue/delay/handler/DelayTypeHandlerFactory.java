package com.faner4cloud.yun.queue.delay.handler;

import com.faner4cloud.yun.queue.delay.DelayTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 延迟队列工厂类
 * @author  faner
 * @since 2022-04-14
 */
@Component
public class DelayTypeHandlerFactory {

    @Autowired
    private final Map<String, DelayTypeHandlerStrategy> DELAY_STRATEGY_MAP = new ConcurrentHashMap<>();

    private final Map<String, String> DELAY_TYPE_MAP = new HashMap<>();

    public DelayTypeHandlerFactory(Map<String, DelayTypeHandlerStrategy> map) {
        this.DELAY_TYPE_MAP.put(DelayTypeEnum.ORDER_PAY_AUTO_CANCEL_DELAY.getBeanType(), DelayTypeEnum.ORDER_PAY_AUTO_CANCEL_DELAY.getBeanType());
        map.forEach(this.DELAY_STRATEGY_MAP::put);
    }

    /**
     * 获取指定的bean
     * @param code
     * @return
     */
    public DelayTypeHandlerStrategy getDelayTypeHandlerStrategy(String code){
        String beanType = DELAY_TYPE_MAP.get(DelayTypeEnum.getDelayTypeHandler(code).getBeanType());
        if(null == beanType){
            return null;
        }
        return DELAY_STRATEGY_MAP.get(beanType);
    }
}
