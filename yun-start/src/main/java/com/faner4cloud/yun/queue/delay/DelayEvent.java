package com.faner4cloud.yun.queue.delay;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @描述 desc
 * @作者 faner
 * @创建时间 2022/4/14 5:01 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DelayEvent {

    /**
     * 延迟队列 {@link DelayTypeEnum}
     */
    private DelayTypeEnum delayTypeEnum;

    /**
     * 延迟时间（默认单位 秒）
     */
    private long delayTime;

    /**
     * 重试次数
     */
    private int retryCount;

    /**
     * 延迟时间单位
     */
    private TimeUnit timeUnit;

    /**
     * 延迟值
     */
    private String value;


    public DelayEvent(DelayTypeEnum delayTypeEnum, String value, long delayTime) {
        this.delayTypeEnum = delayTypeEnum;
        this.delayTime = delayTime;
        this.value = value;
        this.timeUnit = TimeUnit.SECONDS;
    }
}
