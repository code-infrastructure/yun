package com.faner4cloud.yun.queue.delay;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * 延迟队列枚举类
 * @author  faner
 * @since 2022-04-14
 */
@Getter
@AllArgsConstructor
public enum DelayTypeEnum {

    ORDER_PAY_AUTO_CANCEL_DELAY("ORDER_PAY_AUTO_CANCEL_DELAY", "未支付订单自动取消", "orderPayAutoCancelDelayTypeHandler"),
    ;

    private String code;
    private String desc;
    private String beanType;

    public static DelayTypeEnum getDelayTypeHandler(String code) {
        Optional<DelayTypeEnum> optional = Stream.of(DelayTypeEnum.values())
                .filter(delayTypeEnum -> delayTypeEnum.code.equals(code))
                .findAny();
        return optional.orElse(null);
    }

    public static DelayTypeEnum[] getAllDelayType() {
        return DelayTypeEnum.values();
    }

}
