package com.faner4cloud.yun.controller;

import com.faner4cloud.yun.common.util.R;
import com.faner4cloud.yun.service.ThreadPoolService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ...zz
 * @version v1
 * @summary 多线程的用例
 * @since 2022/7/11 12:02 PM
 */
@Slf4j
@RestController
@RequiredArgsConstructor
public class ThreadPoolController {

	private final ThreadPoolService threadPoolService;

	/**
	 * 使用异步注解 @Async
	 * 使用自定义线程池 {@link com.faner4cloud.yun.common.config.TaskExecutorConfiguration}
	 */
	@ApiOperation(value = "使用@Async注解", notes = "使用@Async注解")
	@GetMapping("/v1/thread/async/exec")
	public R<Boolean> asyncExec() {
		threadPoolService.useAsyncExec();
		return R.ok(true);
	}

	/**
	 * 多线程并发执行阻塞等待获取结果
	 */
	@GetMapping("/v1/thread/block/exec")
	public R<Boolean> blockAsyncExec() {
		threadPoolService.blockAsyncExec();
		return R.ok(true);
	}

}
