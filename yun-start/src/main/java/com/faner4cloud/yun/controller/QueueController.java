package com.faner4cloud.yun.controller;

import cn.hutool.json.JSONUtil;
import com.faner4cloud.yun.common.util.R;
import com.faner4cloud.yun.dao.entity.SysDict;
import com.faner4cloud.yun.queue.delay.DelayEvent;
import com.faner4cloud.yun.queue.delay.DelayTypeEnum;
import com.faner4cloud.yun.queue.delay.RedisDelayQueueManager;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@Api(value = "queue", tags = "队列")
@ApiSupport(order = 3)
public class QueueController {

	private final RedisDelayQueueManager redisDelayQueueManager;

	@ApiOperation(value = "redis延迟队列", notes = "redis延迟队列")
	@ApiOperationSupport(order = 10, author = "")
	@GetMapping("/v1/queue/delayInRedis")
	public R<Boolean> delayInRedis() {
		for (int i = 1; i <= 1000; i++) {
			int finalI = i;
			new Thread(() -> {
				SysDict sysDict = new SysDict();
				sysDict.setId((long) finalI);
				redisDelayQueueManager.put(new DelayEvent(DelayTypeEnum.ORDER_PAY_AUTO_CANCEL_DELAY, JSONUtil.toJsonStr(sysDict), 1 * 10L));
			}, String.valueOf(i)).start();
		}
		return R.ok(true);
	}
}
