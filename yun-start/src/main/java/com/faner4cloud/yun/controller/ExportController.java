package com.faner4cloud.yun.controller;

import com.faner4cloud.plugin.excel.annotation.ResponseExcel;
import com.faner4cloud.yun.common.util.progress.Result;
import com.faner4cloud.yun.dao.entity.SysDict;
import com.faner4cloud.yun.service.ExportService;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author ...zz
 * @version v1
 * @summary 导出案例
 * @since 2022/5/16 9:02 AM
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@Api(value = "export", tags = "导出")
@ApiSupport(order = 2)
public class ExportController {

	private final ExportService exportService;

	/**
	 * 异步轮询方式导出
	 * @param key
	 * @return
	 */
	@ApiOperation(value = "异步轮询方式导出")
	@ApiOperationSupport(order = 10, author = "faner")
	@GetMapping("/v1/export/async")
	public Result asyncExportV1(@RequestParam String key) {
		return 	exportService.asyncExport(key);
	}

	/**
	 * 同步导出
	 * @return
	 */
	@ApiOperation(value = "同步导出")
	@ApiOperationSupport(order = 20, author = "faner")
	@GetMapping("/v1/export/sync")
	@ResponseExcel
	public List<SysDict> syncExportV1() {
		return 	exportService.syncExport();
	}
}
