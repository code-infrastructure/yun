package com.faner4cloud.yun.handler.like;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.faner4cloud.yun.handler.like.LikeTypeEnum.COMMENT_LIKE_SET;
import static com.faner4cloud.yun.handler.like.LikeTypeEnum.POST_LIKE_SET;

/**
 * 点赞策略工厂
 */
@Component
public class LikeFactory {

    @Autowired
    private final Map<String, LikeHandler> LIKE_HANDLER_MAP = new ConcurrentHashMap<>();

    private final Map<String, String> LIKE_TYPE_MAP = new HashMap<>();

    public LikeFactory(Map<String, LikeHandler> map) {
        this.LIKE_TYPE_MAP.put(POST_LIKE_SET.getDescription(),POST_LIKE_SET.getDescription());
        this.LIKE_TYPE_MAP.put(COMMENT_LIKE_SET.getDescription(), COMMENT_LIKE_SET.getDescription());
        map.forEach(this.LIKE_HANDLER_MAP::put);
    }

    public LikeHandler getLikeStrategy(Integer type){
        String name = LIKE_TYPE_MAP.get(LikeTypeEnum.getLikeTypeEnum(type).getDescription());
        if(null == name){
           return null;
        }
        return LIKE_HANDLER_MAP.get(name);
    }
}
