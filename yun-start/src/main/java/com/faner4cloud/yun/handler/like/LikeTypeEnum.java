package com.faner4cloud.yun.handler.like;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
@AllArgsConstructor
public enum LikeTypeEnum {
    /**
     * 帖子类型
     */
    POST_LIKE_SET(1,"post_like","post_like_count"),
    /**
     * 评论类型
     */
    COMMENT_LIKE_SET(2, "comment_like","comment_like_count");


    /**
     * 类型
     */
    private final Integer type;

    /**
     * 保存用户点赞数据的key
     */
    private final String description;

    /**
     * 保存用户被点赞数量的key
     */
    private final String cacheCount;


    public static LikeTypeEnum getLikeTypeEnum(Integer type) {
        Optional<LikeTypeEnum> value = Arrays.stream(LikeTypeEnum.values())
                .filter(t -> t.getType().equals(type))
                .findFirst();
        if (value.isPresent()){
            return value.get();
        }
        return LikeTypeEnum.POST_LIKE_SET;
    }
}
