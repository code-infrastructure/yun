package com.faner4cloud.yun.common.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author ...zz
 * @version v1
 * @summary 本地缓存配置
 * @since 2022/7/14 4:28 PM
 */
@Configuration
public class LocalCacheConfig {

	//过期时间
	@Value("${localCache.expireSeconds:60}")
	private Long expireSeconds;
	// 初始的缓存空间大小
	@Value("${localCache.initialCapacity:128}")
	private Integer initialCapacity;
	//缓存的最大条数
	@Value("${localCache.maxSize:5000}")
	private Long maxSize;

	@Bean
	public Cache<String, Object> caffeineCache() {
		return Caffeine.newBuilder()
			// 设置最后一次写入或访问后经过固定时间过期
			.expireAfterAccess(expireSeconds, TimeUnit.SECONDS)
			// 初始的缓存空间大小
			.initialCapacity(initialCapacity)
			// 缓存的最大条数
			.maximumSize(maxSize)
			.build();
	}
}
