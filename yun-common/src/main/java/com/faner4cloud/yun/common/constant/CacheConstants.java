package com.faner4cloud.yun.common.constant;

/**
 * @author ...zz
 * @version v1
 * @summary 缓存的key 常量
 * @since 2022/5/10 4:31 PM
 */
public interface CacheConstants {

	/**
	 * 缓存空数据
	 */
	String EMPTY_CACHE = "{}";
	/**
	 * 字典信息缓存
	 */
	String DICT_DETAILS = "dict_details";

	/**
	 * 异步导出缓存
	 */
	String EXPORT_ASYNC = "export_async";

	/**
	 * 同步导出缓存
	 */
	String EXPORT_SYNC = "export_sync";
}
