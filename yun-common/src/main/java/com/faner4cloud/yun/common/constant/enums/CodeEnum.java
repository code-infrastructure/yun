package com.faner4cloud.yun.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Optional;

/**
 * code码
 * @作者 Faner
 * @创建时间 2022/1/26 10:02
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum CodeEnum{

	/**
	 * 系统未知异常
	 */
	FAIL(CodeEnum.FAILURE_CODE, "系统未知异常"),
	/**
	 * 操作成功
	 */
	SUCCESS(CodeEnum.SUCCESS_CODE, "操作成功"),
	/**
	 * 缺少必要的请求参数
	 */
	PARAM_MISS(CodeEnum.PARAM_MISS_CODE, "缺少必要的请求参数"),
	/**
	 * 请求参数类型错误
	 */
	PARAM_TYPE_ERROR(CodeEnum.PARAM_TYPE_ERROR_CODE, "请求参数类型错误"),
	/**
	 * 请求参数绑定错误
	 */
	PARAM_BIND_ERROR(CodeEnum.PARAM_BIND_ERROR_CODE, "请求参数绑定错误"),
	/**
	 * 参数校验失败
	 */
	PARAM_VALID_ERROR(CodeEnum.PARAM_VALID_ERROR_CODE, "参数校验失败"),
	/**
	 * 404 没找到请求
	 */
	NOT_FOUND(CodeEnum.NOT_FOUND_CODE, "404 没找到请求"),
	/**
	 * 消息不能读取
	 */
	MSG_NOT_READABLE(CodeEnum.MSG_NOT_READABLE_CODE, "消息不能读取"),
	/**
	 * 不支持当前请求方法
	 */
	METHOD_NOT_SUPPORTED(CodeEnum.METHOD_NOT_SUPPORTED_CODE, "不支持当前请求方法"),
	/**
	 * 不支持当前媒体类型
	 */
	MEDIA_TYPE_NOT_SUPPORTED(CodeEnum.MEDIA_TYPE_NOT_SUPPORTED_CODE, "不支持当前媒体类型"),
	/**
	 * 不接受的媒体类型
	 */
	MEDIA_TYPE_NOT_ACCEPT(CodeEnum.MEDIA_TYPE_NOT_ACCEPT_CODE, "不接受的媒体类型"),
	/**
	 * 请求被拒绝
	 */
	REQ_REJECT(CodeEnum.REQ_REJECT_CODE, "请求被拒绝"),
	;

	/**
	 * 通用 异常 code
	 */
	public static final int SUCCESS_CODE = 0;
	public static final int FAILURE_CODE = 1;
	public static final int PARAM_MISS_CODE = 100000;
	public static final int PARAM_TYPE_ERROR_CODE = 100001;
	public static final int PARAM_BIND_ERROR_CODE = 100002;
	public static final int PARAM_VALID_ERROR_CODE = 100003;
	public static final int NOT_FOUND_CODE = 100004;
	public static final int MSG_NOT_READABLE_CODE = 100005;
	public static final int METHOD_NOT_SUPPORTED_CODE = 100006;
	public static final int MEDIA_TYPE_NOT_SUPPORTED_CODE = 100007;
	public static final int MEDIA_TYPE_NOT_ACCEPT_CODE = 100008;
	public static final int REQ_REJECT_CODE = 100009;
	/**
	 * 网关通用 code
	 */
	public static final int BAD_GATEWAY_CODE = 100010;
	public static final int SERVICE_UNAVAILABLE_CODE = 100011;
	public static final int GATEWAY_TIMEOUT_CODE = 100012;
	public static final int GATEWAY_FORBADE_CODE = 100013;
	/**
	 * 通用数据层 code
	 */
	public static final int DATA_NOT_EXIST_CODE = 100100;
	public static final int DATA_EXISTED_CODE = 100101;
	public static final int DATA_ADD_FAILED_CODE = 100102;
	public static final int DATA_UPDATE_FAILED_CODE = 100103;
	public static final int DATA_DELETE_FAILED_CODE = 100104;

	//业务状态码
	private Integer code;
	private String message;

	public static CodeEnum getCodeEnum(Integer code){
		Optional<CodeEnum> value = Arrays.stream(CodeEnum.values())
			.filter(t -> t.getCode().equals(code))
			.findFirst();
		if (value.isPresent()){
			return value.get();
		}
		return CodeEnum.SUCCESS;
	}

}

