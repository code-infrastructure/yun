package com.faner4cloud.yun.common.multicache.support;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * @author ...zz
 * @version v1
 * @summary 多级缓存
 * @since 2022/5/5 5:01 PM
 */
@Data
@AllArgsConstructor
public class CacheMessage implements Serializable {

	private String cacheName;

	private Object key;

}
