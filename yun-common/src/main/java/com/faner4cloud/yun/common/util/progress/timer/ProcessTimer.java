package com.faner4cloud.yun.common.util.progress.timer;

import com.faner4cloud.yun.common.util.progress.Progress;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/10 6:27 PM
 */
public interface ProcessTimer {
	void start(final Progress progress);

	void interrupt();
}
