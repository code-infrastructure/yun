package com.faner4cloud.yun.common.util.progress.timer;

import java.util.Objects;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/10 6:27 PM
 */
public class TimerFactory {
	public static ProcessTimer createTimer(TimerConfig timerConfig) {
		Objects.requireNonNull(timerConfig);
		return timerConfig == null ? new NopeTimer() : new DefaultTimer(timerConfig);
	}
}
