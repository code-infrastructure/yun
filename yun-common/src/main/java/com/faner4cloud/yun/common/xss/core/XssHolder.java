package com.faner4cloud.yun.common.xss.core;

/**
 * @author ...zz
 * @version v1
 * @summary 利用 ThreadLocal 缓存线程间的数据
 * @since 2022/5/4 3:13 PM
 */
class XssHolder {
	private static final ThreadLocal<Boolean> TL = new ThreadLocal<>();

	public static boolean isEnabled() {
		return Boolean.TRUE.equals(TL.get());
	}

	public static void setEnable() {
		TL.set(Boolean.TRUE);
	}

	public static void remove() {
		TL.remove();
	}

}
