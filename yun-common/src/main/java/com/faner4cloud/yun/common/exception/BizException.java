package com.faner4cloud.yun.common.exception;

import com.faner4cloud.yun.common.constant.enums.CodeEnum;
import org.springframework.http.HttpStatus;

/**
 * 失败异常
 * description: BizException
 * date: 2020/11/20 16:12
 * author: faner
 */
public class BizException extends HttpException {
	protected int code = CodeEnum.FAIL.getCode();

	//    protected int httpCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
	protected int httpCode = HttpStatus.OK.value();

	public BizException() {
		super(CodeEnum.FAIL.getCode(), CodeEnum.FAIL.getMessage());
	}

	public BizException(String message) {
		super(message);
	}

	public BizException(int code) {
		super(code, CodeEnum.FAIL.getMessage());
		this.code = code;
	}

	public BizException(int code, String message) {
		super(code, message);
		this.code = code;
	}

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public int getHttpCode() {
		return httpCode;
	}
}
