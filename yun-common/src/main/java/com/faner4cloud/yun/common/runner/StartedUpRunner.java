package com.faner4cloud.yun.common.runner;

import cn.hutool.core.date.DateUtil;
import cn.hutool.system.SystemUtil;
import com.alibaba.excel.util.DateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * description: StartedUpRunner
 * date: 2020/11/23 13:31
 * author: faner
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class StartedUpRunner implements ApplicationRunner {

    private final ConfigurableApplicationContext context;
    private final Environment environment;

    @Override
    public void run(ApplicationArguments args) {
        if (context.isActive()) {
			String banner = "-------------服务启动成功----------------------------\n" +
				"时间：" + DateUtil.now() + "\n" +
				"环境：" + environment.getProperty("spring.profiles.active") + "\n" +
				"服务名称：" + environment.getProperty("spring.application.name") + "\n" +
				"端口号：" + environment.getProperty("server.port") + "\n" +
				"-------------服务启动成功----------------------------";
			System.out.println(banner);
        }
    }
}

