package com.faner4cloud.yun.common.util;


import cn.hutool.core.util.StrUtil;

/**
 * @author ...zz
 * @version v1
 * @summary 文件工具类
 * @since 2022/4/30 2:07 PM
 */
public class FileUtil {
	/**
	 * 获取文件扩展名，不带 .
	 * @param filename 文件名
	 * @return 文件名尾缀      a.png -> png
	 */
	public static String getExtensionName(String filename) {
		int index = filename.lastIndexOf('.');
		return filename.substring(index+1);
	}

	/**
	 * 获取文件扩展名，带 .
	 * @param filename 文件名
	 * @return 文件名尾缀   a.png -> .png
	 */
	public static String getFileExt(String filename) {
		int index = filename.lastIndexOf('.');
		return filename.substring(index);
	}

	/**
	 * Java文件操作 获取不带扩展名的文件名
	 * @param filename 文件名
	 *
	 * @return 文件名不包括尾缀 a.png -> a
	 */
	public static String getFileNameNoEx(String filename) {
		return  getFileNameNoEx(filename,".");
	}

	/**
	 * Java文件操作 获取不带扩展名的文件名
	 * @param filename 文件名
	 * @param separatorChars 分隔符
	 *
	 * @return 文件名不包括尾缀
	 */
	public static String getFileNameNoEx(String filename,String separatorChars) {
		if (StrUtil.isEmpty(separatorChars)){
			separatorChars = ".";
		}
		int index = filename.lastIndexOf(separatorChars);
		return filename.substring(0, index);
	}


	/**
	 * 获取文件名
	 * @param filename 文件名
	 *
	 * @return 文件名不包括尾缀 https://ke.com/test/2022/4/a.png -> a.png
	 */
	public static String getFileName(String filename) {
		int index = filename.lastIndexOf('/');
		return filename.substring(index+1);
	}

}
