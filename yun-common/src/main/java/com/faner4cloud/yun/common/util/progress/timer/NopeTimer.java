package com.faner4cloud.yun.common.util.progress.timer;

import com.faner4cloud.yun.common.util.progress.Progress;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/10 6:28 PM
 */
class NopeTimer implements ProcessTimer {
	@Override
	public void start(Progress progress) {
		return;
	}

	@Override
	public void interrupt() {
		return;
	}
}
