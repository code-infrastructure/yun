package com.faner4cloud.yun.common.util;

import com.faner4cloud.yun.common.constant.enums.CodeEnum;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 响应信息主体
 *
 * @param <T>
 * @作者 Faner
 * @创建时间 2022/1/26 9:59
 */
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class R<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int code;

	@Getter
	@Setter
	private String msg;

	@Getter
	@Setter
	private String path;


	@Getter
	@Setter
	private T data;

	public static <T> R<T> ok() {
		return restResult(null, CodeEnum.SUCCESS.getCode(), null);
	}

	public static <T> R<T> ok(T data) {
		return restResult(data, CodeEnum.SUCCESS.getCode(), null);
	}

	public static <T> R<T> ok(T data, String msg) {
		return restResult(data, CodeEnum.SUCCESS.getCode(), msg);
	}

	public static <T> R<T> failed() {
		return restResult(null, CodeEnum.FAIL.getCode(), null);
	}


	public static <T> R<T> failed(String msg) {
		return restResult(null, CodeEnum.FAIL.getCode(), msg);
	}

	public static <T> R<T> failed(T data) {
		return restResult(data, CodeEnum.FAIL.getCode(), null);
	}


	public static <T> R<T> failed(T data, String msg) {
		return restResult(data, CodeEnum.FAIL.getCode(), msg);
	}

	public static <T> R<T> restResult(T data, int code, String msg) {
		R<T> apiResult = new R<>();
		apiResult.setCode(code);
		apiResult.setData(data);
		apiResult.setMsg(msg);
		apiResult.setPath(RequestUtil.getSimpleRequest());
		return apiResult;
	}

}
