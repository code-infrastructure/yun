package com.faner4cloud.yun.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
/**
 * @author ...zz
 * @version v1
 * @summary 设备类型 针对一套 用户体系
 * @since 2022/8/17 10:20 AM
 */
@Getter
@AllArgsConstructor
public enum DeviceTypeEnum {

	/**
	 * pc端
	 */
	PC("pc"),

	/**
	 * app端
	 */
	APP("app"),

	/**
	 * 小程序端
	 */
	XCX("xcx");

	private final String device;
}

