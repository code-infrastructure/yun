package com.faner4cloud.yun.common.redis.cache;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import org.apache.poi.util.StringUtil;

import javax.annotation.Nullable;
import java.time.Duration;

/**
 * cache key
 *
 * @author L.cm
 */
public interface ICacheKey {

	/**
	 * 获取前缀
	 *
	 * @return key 前缀
	 */
	String getPrefix();

	/**
	 * 超时时间
	 *
	 * @return 超时时间
	 */
	@Nullable
	default Duration getExpire() {
		return null;
	}

	/**
	 * 组装 cache key
	 *
	 * @param suffix 参数
	 * @return cache key
	 */
	default String getKeyStr(Object... suffix) {
		String prefix = this.getPrefix();
		// 拼接参数
		if (ObjectUtil.isEmpty(suffix)) {
			return prefix;
		}
		return prefix.concat(StringUtil.join(suffix, StringPool.COLON));
	}

	/**
	 * 组装 cache key
	 *
	 * @param suffix 参数
	 * @return cache key
	 */
	default CacheKey getKey(Object... suffix) {
		String key = this.getKeyStr(suffix);
		Duration expire = this.getExpire();
		return expire == null ? new CacheKey(key) : new CacheKey(key, expire);
	}

}
