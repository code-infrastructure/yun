package com.faner4cloud.yun.common.constant.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author ...zz
 * @version v1
 * @summary  字典类型
 * @since 2022/5/10 4:33 PM
 */
@Getter
@RequiredArgsConstructor
public enum DictTypeEnum {
	/**
	 * 字典类型-系统内置（不可修改）
	 */
	SYSTEM("1", "系统内置"),

	/**
	 * 字典类型-业务类型
	 */
	BIZ("0", "业务类");

	/**
	 * 类型
	 */
	private final String type;

	/**
	 * 描述
	 */
	private final String description;
}
