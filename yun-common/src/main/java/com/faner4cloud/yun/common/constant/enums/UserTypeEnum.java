package com.faner4cloud.yun.common.constant.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author ...zz
 * @version v1
 * @summary 设备类型 针对多套 用户体系
 * @since 2022/8/17 10:07 AM
 */
@Getter
@AllArgsConstructor
public enum UserTypeEnum {

	/**
	 * pc端
	 */
	SYS_USER("sys_user"),

	/**
	 * app端
	 */
	APP_USER("app_user");

	private final String userType;

	public static UserTypeEnum getUserType(String str) {
		for (UserTypeEnum value : values()) {
			if (StrUtil.contains(str, value.getUserType())) {
				return value;
			}
		}
		throw new RuntimeException("'UserType' not found By " + str);
	}
}
