package com.faner4cloud.yun.common.util;

import org.springframework.stereotype.Component;

import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ...zz
 * @version v1
 * @summary 限流
 * @since 2022/5/19 9:53 AM
 */
@Component
public class RateLimitUtil {

	private final AtomicInteger initPermits = new AtomicInteger(2);

	private final Semaphore semaphore = new Semaphore(initPermits.get());

	public boolean tryAcquire() {
		return semaphore.tryAcquire();
	}

	public void release() {
		semaphore.release();
	}

	public int availablePermits() {
		return semaphore.availablePermits();
	}

	public int currentRunSize() {
		return initPermits.get() - semaphore.availablePermits();
	}

	public void add(int permits) {
		initPermits.addAndGet(permits);
		semaphore.release(permits);
	}

	public int getPermits() {
		return initPermits.get();
	}

}

///**
// * 限流
// */
//public class BatchRateLimit{
//	private final AtomicInteger initPermits = new AtomicInteger(2);
//
//	private final AtomicInteger count = new AtomicInteger();
//
//	private final Semaphore semaphore = new Semaphore(initPermits.get());
//
//	public boolean tryAcquire(int signUpCount) {
//		if (semaphore.tryAcquire()) {
//			count.addAndGet(signUpCount);
//			return true;
//		} else {
//			return false;
//		}
//	}
//
//	public void release(int signUpCount) {
//		semaphore.release(signUpCount);
//		count.addAndGet(-signUpCount);
//	}
//
//	public int availablePermits() {
//		return semaphore.availablePermits();
//	}
//
//	public int currentRunSize() {
//		return count.get();
//	}
//
//	public void add(int permits) {
//		initPermits.addAndGet(permits);
//		semaphore.release(permits);
//	}
//
//	public int getPermits() {
//		return initPermits.get();
//	}
//}
