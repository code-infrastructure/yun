package com.faner4cloud.yun.common.redis.lock;

/**
 * @author ...zz
 * @version v1
 * @summary redis 分布式锁工具类
 * @since 2022/5/5 5:01 PM
 */
public interface IRedisLock {

	/**
	 * 加锁默认失效时间5s
	 * @param key
	 * @return true/false
	 */
	boolean tryLock(String key);

	/**
	 * 加锁默认只获取一次
	 * @param key
	 * @param expire 过期时间
	 * @return true/false
	 */
	boolean tryLock(String key, long expire);

	/**
	 *  加锁默认只获取一次
	 * @param key
	 * @param expire 过期时间
	 * @param retryTimes 重试次数
	 * @param sleepMillis 阻塞时间
	 * @return true/false
	 */
	boolean tryLock(String key, long expire, int retryTimes, long sleepMillis);

	/**
	 * 解锁
	 * @param key
	 */
	void releaseLock(String key);
}
