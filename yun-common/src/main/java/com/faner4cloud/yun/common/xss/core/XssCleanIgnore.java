package com.faner4cloud.yun.common.xss.core;

import java.lang.annotation.*;

/**
 * @author ...zz
 * @version v1
 * @summary 忽略 xss
 * @since 2022/5/4 2:29 PM
 */
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface XssCleanIgnore {
}
