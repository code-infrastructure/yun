package com.faner4cloud.yun.common.runner;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ...zz
 * @version v1
 * @summary 服务启动后操作
 * @since 2022/4/30 2:02 PM
 */
@Slf4j
@Component
public class StartUpOpsListener implements ApplicationListener<ContextRefreshedEvent> {

	private final AtomicInteger atomNo = new AtomicInteger(1);

	/**
	 * Handle an application event.
	 *
	 * @param event the event to respond to
	 */
	@SuppressWarnings("NullableProblems")
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.info("******服务启动-[StartUpOps]-刷新第:{}次******", atomNo.getAndIncrement());
		int maxRefresh = 21;
		if (atomNo.get() == maxRefresh) {
			log.info("******服务启动-[StartUpOps]-refresh次数:{}******", maxRefresh);
			InetAddress addr;
			try {
				addr = InetAddress.getLocalHost();
				log.info("本地ip为: {}", addr.getHostAddress());
				String hostname = addr.getHostName();
				log.info("本地name为: {}", hostname);
			} catch (Exception e) {
				log.error("#InetAddress异常#", e);
			}

		}
	}
}
