package com.faner4cloud.yun.common.constant;

/**
 * 缓存相关的
 */
public class CommonConstants {

    /**
     * 菜单树根节点
     */
    public static Long MENU_TREE_ROOT_ID = -1L;

    /**
     * 默认分页数量
     */
    public static Long DEFAULT_PAGE_SIZE = 20L;

	/**
	 * 管理员ID
	 */
	public static Long ADMIN_ID = 1L;
}
