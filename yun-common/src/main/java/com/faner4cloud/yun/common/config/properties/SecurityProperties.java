package com.faner4cloud.yun.common.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author ...zz
 * @version v1
 * @summary Security 配置属性
 * @since 2022/8/12 9:47 AM
 */
@Data
@Component
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {

	/**
	 * 排除路径
	 */
	private String[] excludes;

}
