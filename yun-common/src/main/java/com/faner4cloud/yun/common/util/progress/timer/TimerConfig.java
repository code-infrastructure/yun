package com.faner4cloud.yun.common.util.progress.timer;

import com.faner4cloud.yun.common.util.progress.Progress;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * @author ...zz
 * @version v1
 * @summary desc
 * @since 2022/5/10 6:25 PM
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimerConfig {
	private  TimeUnit unit;
	private  long interval;
	private  Consumer<Progress> timingConsumer;
}
