package com.faner4cloud.yun.common.util.progress;

public class Result {
	/**
	 * 进度标识
	 */
	private String identity = ProgressBuilder.IDENTITY_UNDEFINED;

	/**
	 * 当前执行步骤名
	 */
	private String stepName = TaskBuilder.TASKNAME_UNDEFINED;

	/**
	 * 进度执行结果
	 */
	private Object result;

	/**
	 * 当前进度
	 */
	private double process = 0D;

	/**
	 * 进度执行状态
	 */
	private ProgressState state = ProgressState.NEW;

	public Result() {
	}

	Result fresh(String identity, String stepName, ProgressState state, Object result, double process) {
		this.identity = identity;
		this.stepName = stepName;
		this.state = state;
		this.result = result;
		this.process = process;
		return this;
	}

	/**
	 * 获得 进度的唯一标识
	 *
	 * @return String
	 * @author Jail.Hu
	 */
	public String getIdentity() {
		return identity;
	}

	/**
	 * 获得当前进度执行步骤名
	 *
	 * @return String
	 * @author Jail.Hu
	 */
	public String getStepName() {
		return stepName;
	}

	/**
	 * 获得进度执行结果
	 *
	 * @return Object
	 */
	public Object getResult() {
		return result;
	}

	/**
	 * 获得当前执行进度,百分比
	 *
	 * @return double
	 */
	public double getProcess() {
		return process;
	}

	/**
	 * 获得当前的进度执行状态
	 *
	 * @return ProgressState
	 */
	public ProgressState getState() {
		return this.state;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public void setProcess(double process) {
		this.process = process;
	}

	public void setState(ProgressState state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Result [identity=" + identity + ", stepName=" + stepName + ", state=" + state + ", result=" + result + ", process=" + process + "]";
	}
}
