package com.faner4cloud.yun.common.util;

import cn.hutool.core.convert.Convert;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * description: RequestUtil
 * date: 2020/11/20 13:53
 * author: faner
 */
public class RequestUtil {
    /**
     * 获得当前请求
     *
     * @return Request 对象，如果没有绑定会返回 null
     */
    public static HttpServletRequest getRequest() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        // 当前线程没有绑定 Request
        if (requestAttributes == null) {
            return null;
        }

        if (requestAttributes instanceof ServletRequestAttributes) {
            ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) requestAttributes;
            return servletRequestAttributes.getRequest();
        } else {
            return null;
        }
    }

    /**
     * 获得请求 url
     *
     * @return url
     */
    public static String getRequestUrl() {
        HttpServletRequest request = RequestUtil.getRequest();
        if (request == null) {
            return null;
        }
        return request.getServletPath();
    }

    /**
     * 获得请求简略信息
     *
     * @param request 请求
     * @return 简略信息
     */
    public static String getSimpleRequest(HttpServletRequest request) {
        return request.getMethod() + " " + request.getServletPath();
    }

    /**
     * 获得请求简略信息
     *
     * @return 简略信息
     */
    public static String getSimpleRequest() {
        HttpServletRequest request = getRequest();
        if (request == null) {
            return null;
        }
        return request.getMethod() + " " + request.getServletPath();
    }

    /**
     * 获取String参数
     */
    public static String getParameter(String name) {
		HttpServletRequest request = RequestUtil.getRequest();
		if (request == null) {
			return null;
		}
    	return request.getParameter(name);
    }

    /**
     * 获取String参数
     */
    public static String getParameter(String name, String defaultValue) {
		HttpServletRequest request = RequestUtil.getRequest();
		if (request == null) {
			return null;
		}
        return Convert.toStr(request.getParameter(name), defaultValue);
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name) {
		HttpServletRequest request = RequestUtil.getRequest();
		if (request == null) {
			return null;
		}
    	return Convert.toInt(request.getParameter(name));
    }

    /**
     * 获取Integer参数
     */
    public static Integer getParameterToInt(String name, Integer defaultValue) {
		HttpServletRequest request = RequestUtil.getRequest();
		if (request == null) {
			return null;
		}
        return Convert.toInt(request.getParameter(name), defaultValue);
    }

    /**
     * 获取Boolean参数
     */
    public static Boolean getParameterToBool(String name) {
		HttpServletRequest request = RequestUtil.getRequest();
		if (request == null) {
			return null;
		}
    	return Convert.toBool(request.getParameter(name));
    }

    /**
     * 获取Boolean参数
     */
    public static Boolean getParameterToBool(String name, Boolean defaultValue) {
		HttpServletRequest request = RequestUtil.getRequest();
		if (request == null) {
			return null;
		}
        return Convert.toBool(request.getParameter(name), defaultValue);
    }

}
