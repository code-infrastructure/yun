package com.faner4cloud.yun.common.xss.core;

/**
 * @author ...zz
 * @version v1
 * @summary xss 清理器
 * @since 2022/5/4 2:41 PM
 */
public interface XssCleaner {

	/**
	 * 清理 html
	 *
	 * @param html html
	 * @return 清理后的数据
	 */
	String clean(String html);

}
