package com.faner4cloud.yun.common.util.progress;

import com.faner4cloud.yun.common.util.progress.AbstractTask.AbstractStepTask;
import com.faner4cloud.yun.common.util.progress.AbstractTask.Prepare;

import java.util.function.Function;


public final class TaskBuilder {

	/**
	 * 未定义的任务名
	 */
	final static String TASKNAME_UNDEFINED = "undefined";

	public TaskBuilder() {
		super();
	}

	/**
	 * 准备数据步骤
	 *
	 * @param rate     准备数据步骤所占的进度比重
	 * @param taskName 步骤名
	 * @param func     准备数据执行的方法
	 * @return Prepare
	 */
	public static <O> AbstractTask.Prepare<O, O> prepare(int rate, String taskName, Function<AbstractStepTask<?, ?>, O> func) {
		Prepare<O, O> prepare = new Prepare<O, O>(rate, (O o, AbstractStepTask<?, ?> task) -> func.apply(task), taskName);
		return prepare;
	}

	/**
	 * 准备数据, <b>步骤名默认为 {@link ProgressBuilder#TASKNAME_UNDEFINED}</b>
	 *
	 * @param rate 进度比重
	 * @param func 准备数据执行的方法
	 * @return Prepare
	 */
	public static <O> Prepare<O, O> prepare(int rate, Function<AbstractStepTask<?, ?>, O> func) {
		return prepare(rate, TASKNAME_UNDEFINED, func);
	}
}
