package com.faner4cloud.yun.common.exception;

import com.faner4cloud.yun.common.constant.enums.CodeEnum;
import org.springframework.http.HttpStatus;

/**
 * HttpException 异常类
 * 含异常信息 message
 * http状态码 httpCode
 * 错误码 code
 * description: HttpException
 * date: 2020/11/20 16:00
 * author: faner
 */
public class HttpException extends RuntimeException implements BaseResponse {


    private static final long serialVersionUID = 2359767895161832954L;

    protected int httpCode = HttpStatus.INTERNAL_SERVER_ERROR.value();

    protected int code = CodeEnum.FAIL.getCode();

    public HttpException() {
        super(CodeEnum.FAIL.getMessage());
    }

    public HttpException(String message) {
        super(message);
    }

    public HttpException(int code) {
        super(CodeEnum.FAIL.getMessage());
        this.code = code;
        this.httpCode =  HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public HttpException(int code, int httpCode) {
        super(CodeEnum.FAIL.getMessage());
        this.httpCode = httpCode;
        this.code = code;
    }
    public HttpException(int code, String message) {
        super(message);
        this.code = code;
        this.httpCode =  HttpStatus.INTERNAL_SERVER_ERROR.value();
    }

    public HttpException(int code, String message, int httpCode) {
        super(message);
        this.code = code;
        this.httpCode = httpCode;
    }

    public HttpException(Throwable cause, int code) {
        super(cause);
        this.code = code;
    }

    public HttpException(Throwable cause, int code, int httpCode) {
        super(cause);
        this.code = code;
        this.httpCode = httpCode;
    }

    public HttpException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * for better performance
     *
     * @return Throwable
     */
    public Throwable doFillInStackTrace() {
        return super.fillInStackTrace();
    }

    @Override
    public int getHttpCode() {
        return code;
    }

    @Override
    public int getCode() {
        return httpCode;
    }
}
