package com.faner4cloud.yun.common.exception;

/**
 * 基础的返回接口
 * 必须实现以下三个方法
 * description: BaseResponse
 * date: 2020/11/20 16:04
 * author: faner
 */

public interface BaseResponse {

    /**
     * 返回的信息
     *
     * @return 返回的信息
     */
    String getMessage();

    /**
     * http 状态码
     *
     * @return http 状态码
     */
    int getHttpCode();

    /**
     * 错误码
     *
     * @return 错误码
     */
    int getCode();
}
