package com.faner4cloud.yun.common.util.progress;

/**
 * @author ...zz
 * @version v1
 * @summary 进度状态
 * @since 2022/5/10 6:29 PM
 */
public enum ProgressState {

	/**
	 * 新建
	 */
	NEW(0),
	/**
	 * 进行中
	 */
	DOING(1),
	/**
	 * 完成
	 */
	DONE(2),
	/**
	 * 取消
	 */
	CANCELED(4),
	/**
	 * 失败
	 */
	FAILED(5);

	private final int flag;

	private ProgressState(int flag) {
		this.flag = flag;
	}

	int getFlag() {
		return this.flag;
	}

	/**
	 * 进度是否已结束（含 {@link ProgressState#DONE} , {@link ProgressState#FAILED} , {@link ProgressState#CANCELED}）
	 *
	 * @param state 当前进度状态
	 * @return boolean
	 * @date 2019年4月4日 下午4:30:21
	 */
	public static boolean isFinished(ProgressState state) {
		return state.getFlag() >= ProgressState.DONE.getFlag();
	}
}
