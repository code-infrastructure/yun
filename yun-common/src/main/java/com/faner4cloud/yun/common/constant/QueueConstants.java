package com.faner4cloud.yun.common.constant;

/**
 * description: QueueConstants
 * date: 2020/12/4 13:04
 * author: faner
 */
public class QueueConstants {

	public static String QUEUE_SAVE = "save";
	public static String QUEUE_DELETE = "delete";

}
