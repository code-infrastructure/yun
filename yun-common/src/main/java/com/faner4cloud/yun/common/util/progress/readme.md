

```java

// 构建 Progress 类
Progress progress = ProgressBuilder.identity(sign) // sign 为Progress 名，用于系统控制重复任务
  .timing(TimeUnit.SECONDS, 5, p -> {
    // 定时器，在任务未完成前 每 interval 个 unit 执行一次， 入参 当前 Progress;
    // 在分布式应用场景中，一般在这里写执行进度到分布式缓存
  })
  .prepare(10, "数据准备", (task) -> {
    // 10 为分步任务的占比权重， 比如共4steps，每个step的权重都是10， 那么每个task = 25%占比；
    // "数据准备" 为当前step名
    // supplier ， 数据提供函数， 入参为当前执行的 step-task， 出参为下一个函数的入参
    return xxx;
  }
  .next(10, "数据填充", (list, task) -> {
    // 10 为分步任务的占比权重， 比如共4steps，每个step的权重都是10， 那么每个task = 25%占比；
    // "数据填充" 为当前step名
    // function， 数据处理步骤函数， 入参为上一个函数的出参与当前执行的 step-task， 出参为下一个函数的入参
    task.mark(done, total);
    // task.mark 用于更新当前步骤的细化进度，可以根据自己的需求更新步骤进度
    return xxx;
  }).next(10, "excel处理", (datas, task) -> {
    // 10 为分步任务的占比权重， 比如共4steps，每个step的权重都是10， 那么每个task = 25%占比；
    // "excel处理" 为当前step名
    // function， 数据处理步骤函数， 入参为上一个函数的出参与当前执行的 step-task， 出参为下一个函数的入参
    task.mark(done, total);
    // task.mark 用于更新当前步骤的细化进度，可以根据自己的需求更新步骤进度
    return xxx;
  }).afterFinish("缓存更新", r -> {
    // 在所有步骤执行完毕后执行的 consumer
    // 此环节一般用于写分布式缓存，留log等
  }).build();
// 开始执行 ，使用线程池或非线程池
progress.run(executorService);
// 获得任务结果
progress.get();
```
