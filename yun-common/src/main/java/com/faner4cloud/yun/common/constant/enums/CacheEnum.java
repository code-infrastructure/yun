package com.faner4cloud.yun.common.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.concurrent.TimeUnit;

/**
 * @描述 缓存相关的
 * @作者 faner
 * @创建时间 2022/4/21 6:21 PM
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public enum CacheEnum {
	//缓存前缀与缓存时间
	CACHE_2MINS("yun:cache:2m", "2分钟缓存组", TimeUnit.MINUTES.toSeconds(2)),
	CACHE_5MINS("yun:cache:5m", "5分钟缓存组", TimeUnit.MINUTES.toSeconds(5)),
	CACHE_15MINS("yun:cache:15m", "15分钟缓存组", TimeUnit.MINUTES.toSeconds(15)),
	CACHE_30MINS("yun:cache:30m", "30分钟缓存组", TimeUnit.MINUTES.toSeconds(30)),
	CACHE_60MINS("yun:cache:60m", "60分钟缓存组", TimeUnit.MINUTES.toSeconds(60)),
	CACHE_180MINS("yun:cache:180m", "180分钟缓存组", TimeUnit.MINUTES.toSeconds(180)),
	CACHE_24HOURS("yun:cache:24h", "24小时缓存组", TimeUnit.HOURS.toSeconds(24)),

	CACHE_DICT_DETAILS(CacheEnum.CACHE_24HOURS.getKey()+":dict_details","字典信息缓存",CacheEnum.CACHE_24HOURS.getExpireTime()),
	;

	private String key;

	private String desc;

	private long expireTime;


}

