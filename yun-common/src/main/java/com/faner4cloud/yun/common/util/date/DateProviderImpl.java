package com.faner4cloud.yun.common.util.date;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author ...zz
 * @version v1
 * @summary 日期辅助组件接口
 * @since 2022/6/27 3:10 PM
 */
@Slf4j
public class DateProviderImpl implements DateProvider {
	/**
	 * 获取当前时间
	 *
	 * @return 当前时间
	 */
	@Override
	public Date getCurrentTime() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(DatePattern.NORM_DATETIME_PATTERN);
		try {
			return dateFormatter.parse(dateFormatter.format(new Date()));
		} catch (Exception e) {
			log.warn("getCurrentTime error");
		}
		return null;
	}

	/**
	 * 将Date对象格式化成：yyyy-MM-dd HH:mm:ss
	 *
	 * @param date Date对象
	 * @return 格式化日期字符串
	 */
	@Override
	public String formatDatetime(Date date) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(DatePattern.NORM_DATETIME_PATTERN);
		return dateFormatter.format(date);
	}

	/**
	 * 将日期字符串转化为Date对象
	 *
	 * @param datetime 日期字符串
	 * @return date对象
	 */
	@Override
	public Date parseDatetime(String datetime) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(DatePattern.NORM_DATETIME_PATTERN);
		try {
			return dateFormatter.parse(datetime);
		} catch (Exception e) {
			log.warn("parseDatetime error");
		}
		return null;

	}

	public static void main(String[] args) {
		System.out.println(DateUtil.parse("2022-10-11 10:10:10"));
	}
}
